package cn.bugstack.chatglm.executor;

import cn.bugstack.chatglm.model.*;
import okhttp3.sse.EventSource;
import okhttp3.sse.EventSourceListener;

import java.io.IOException;

public interface Executor {
    ChatCompletionResponse chatCompletion(ChatCompletionRequest chatCompletionRequest) throws IOException;

    EventSource completions(ChatCompletionRequest chatCompletionRequest, EventSourceListener eventSourceListener) throws IOException;

    ImageCompletionResponse genImages(ImageCompletionRequest imageCompletionRequest) throws IOException;;

    VideoCompletionResponse genVideos(VideoCompletionRequest videoCompletionRequest) throws Exception;
}
