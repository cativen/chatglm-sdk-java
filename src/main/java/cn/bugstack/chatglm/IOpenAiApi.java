package cn.bugstack.chatglm;

public interface IOpenAiApi {
    String v4 = "api/paas/v4/chat/completions";
    String v4Image = "api/paas/v4/images/generations";
    String VIDEO = "api/paas/v4/videos/generations";
    String GET_VIDEO_RES = "api/paas/v4/async-result/";
}
