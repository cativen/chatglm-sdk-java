package cn.bugstack.chatglm.session;

import cn.bugstack.chatglm.model.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import okhttp3.sse.EventSource;
import okhttp3.sse.EventSourceListener;

import java.io.IOException;

public interface OpenSession {
    /**
     * 简单的对话
     * @param chatCompletionRequest
     * @return
     * @throws IOException
     */
    ChatCompletionResponse chatCompletion(ChatCompletionRequest chatCompletionRequest) throws IOException;

    /**
     * 文本问答 & 流式反馈
     * @param chatCompletionRequest 请求信息
     * @param eventSourceListener 实现监听；通过监听的 onEvent 方法接收数据
     */
    EventSource completions(ChatCompletionRequest chatCompletionRequest, EventSourceListener eventSourceListener) throws Exception;

    /**
     * 图片生成
     * @param imageCompletionRequest
     * @return
     * @throws IOException
     */
    ImageCompletionResponse genImages(ImageCompletionRequest imageCompletionRequest) throws IOException;


    /**
     * 视频生成
     * @param videoCompletionRequest
     * @return
     * @throws IOException
     */
    VideoCompletionResponse genVideos(VideoCompletionRequest videoCompletionRequest) throws Exception;
}
