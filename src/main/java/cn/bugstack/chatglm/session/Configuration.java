package cn.bugstack.chatglm.session;

import cn.bugstack.chatglm.executor.Executor;
import cn.bugstack.chatglm.executor.aigc.GLMExecutor;
import cn.bugstack.chatglm.model.Model;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import okhttp3.sse.EventSource;
import okhttp3.sse.EventSources;

import java.util.HashMap;

@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public class Configuration {

    // 智普Ai ChatGlM 请求地址
    @Getter
    @Setter
    private String apiKey ;

    @Getter
    @Setter
    private String apiHost = "https://open.bigmodel.cn/";

    @Getter
    @Setter
    private String apiSecret;

    @Getter
    private String apiSecretKey;

    @Getter
    @Setter
    private OkHttpClient okHttpClient;

    @Setter
    @Getter
    private long connectTimeout = 450;
    @Setter
    @Getter
    private long writeTimeout = 450;
    @Setter
    @Getter
    private long readTimeout = 450;

    private HashMap<Model, Executor> executorGroup;


    public static final String DEFAULT_USER_AGENT = "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)";
    public static final String APPLICATION_JSON = "application/json";
    public static final String JSON_CONTENT_TYPE = APPLICATION_JSON + "; charset=utf-8";

    public void setApiSecretKey(String apiSecretKey) {
        this.apiSecretKey = apiSecretKey;
        if (null != apiSecretKey) {
            String[] split = apiSecretKey.split("\\.");
            this.apiKey = split[0];
            this.apiSecret = split[1];
        }
    }


    @Getter
    @Setter
    private OkHttpClient httpClient;

    // OkHttp 配置信息
    @Setter
    @Getter
    private HttpLoggingInterceptor.Level level = HttpLoggingInterceptor.Level.HEADERS;

    public EventSource.Factory createRequestFactory() {
        return EventSources.createFactory(okHttpClient);
    }
    public HashMap<Model, Executor> newExecutorGroup() {
        this.executorGroup = new HashMap<>();
        GLMExecutor glmExecutor = new GLMExecutor(this);
        executorGroup.put(Model.GLM_4V_PLUS, glmExecutor);
        executorGroup.put(Model.COGVIEW_3_PLUS, glmExecutor);
        executorGroup.put(Model.COGVIDEOX, glmExecutor);
        return executorGroup;
    }

}
