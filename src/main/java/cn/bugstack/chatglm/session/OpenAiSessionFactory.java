package cn.bugstack.chatglm.session;

public interface OpenAiSessionFactory {
    OpenSession openSession();
}
