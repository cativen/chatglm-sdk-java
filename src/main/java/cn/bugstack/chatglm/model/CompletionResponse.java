package cn.bugstack.chatglm.model;
import java.util.Arrays;
import java.util.List;

public class CompletionResponse {
    private String id;
    private long created;
    private String model;
    private List<Choice> choices;

    // 构造函数
    public CompletionResponse(String id, long created, String model, List<Choice> choices) {
        this.id = id;
        this.created = created;
        this.model = model;
        this.choices = choices;
    }

    // Getter和Setter方法
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public List<Choice> getChoices() {
        return choices;
    }

    public void setChoices(List<Choice> choices) {
        this.choices = choices;
    }

    // 内部类Choice
    public static class Choice {
        private int index;
        private Delta delta;

        // 构造函数
        public Choice(int index, Delta delta) {
            this.index = index;
            this.delta = delta;
        }

        // Getter和Setter方法
        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public Delta getDelta() {
            return delta;
        }

        public void setDelta(Delta delta) {
            this.delta = delta;
        }

        // toString方法
        @Override
        public String toString() {
            return "Choice{" +
                    "index=" + index +
                    ", delta=" + delta +
                    '}';
        }
    }

    // 内部类Delta
    public static class Delta {
        private String role;
        private String content;

        // 构造函数
        public Delta(String role, String content) {
            this.role = role;
            this.content = content;
        }

        // Getter和Setter方法
        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        // toString方法
        @Override
        public String toString() {
            return "Delta{" +
                    "role='" + role + '\'' +
                    ", content='" + content + '\'' +
                    '}';
        }
    }

    // toString方法
    @Override
    public String toString() {
        return "CompletionResponse{" +
                "id='" + id + '\'' +
                ", created=" + created +
                ", model='" + model + '\'' +
                ", choices=" + choices +
                '}';
    }

    // 主方法，用于测试
    public static void main(String[] args) {
        Delta delta = new Delta("assistant", "，");
        Choice choice = new Choice(0, delta);
        CompletionResponse response = new CompletionResponse("2025012209475906695f316f6d4590", 1737510479, "glm-4v-plus", Arrays.asList(choice));

        System.out.println(response);
    }
}

