package cn.bugstack.chatglm.model;

import java.util.List;

public class ChatCompletionResponse {
    private String content;
    private String role;
    private List<Choice> choices;
    private long created;
    private String id;
    private String model;
    private String request_id;
    private Usage usage;

    // 构造函数
    public ChatCompletionResponse(String content, String role, List<Choice> choices, long created, String id, String model, String request_id, Usage usage) {
        this.content = content;
        this.role = role;
        this.choices = choices;
        this.created = created;
        this.id = id;
        this.model = model;
        this.request_id = request_id;
        this.usage = usage;
    }

    // Getter和Setter方法
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<Choice> getChoices() {
        return choices;
    }

    public void setChoices(List<Choice> choices) {
        this.choices = choices;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public Usage getUsage() {
        return usage;
    }

    public void setUsage(Usage usage) {
        this.usage = usage;
    }

    // 内部类Choice
    public static class Choice {
        private String finish_reason;
        private int index;
        private Message message;

        // 构造函数
        public Choice(String finish_reason, int index, Message message) {
            this.finish_reason = finish_reason;
            this.index = index;
            this.message = message;
        }

        // Getter和Setter方法
        public String getFinish_reason() {
            return finish_reason;
        }

        public void setFinish_reason(String finish_reason) {
            this.finish_reason = finish_reason;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public Message getMessage() {
            return message;
        }

        public void setMessage(Message message) {
            this.message = message;
        }
    }

    // 内部类Message
    public static class Message {
        private String content;
        private String role;

        // 构造函数
        public Message(String content, String role) {
            this.content = content;
            this.role = role;
        }

        // Getter和Setter方法
        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }
    }

    // 内部类Usage
    public static class Usage {
        private int completion_tokens;
        private int prompt_tokens;
        private int total_tokens;

        // 构造函数
        public Usage(int completion_tokens, int prompt_tokens, int total_tokens) {
            this.completion_tokens = completion_tokens;
            this.prompt_tokens = prompt_tokens;
            this.total_tokens = total_tokens;
        }

        // Getter和Setter方法
        public int getCompletion_tokens() {
            return completion_tokens;
        }

        public void setCompletion_tokens(int completion_tokens) {
            this.completion_tokens = completion_tokens;
        }

        public int getPrompt_tokens() {
            return prompt_tokens;
        }

        public void setPrompt_tokens(int prompt_tokens) {
            this.prompt_tokens = prompt_tokens;
        }

        public int getTotal_tokens() {
            return total_tokens;
        }

        public void setTotal_tokens(int total_tokens) {
            this.total_tokens = total_tokens;
        }
    }

    // toString方法
    @Override
    public String toString() {
        return "SocialismInfo{" +
                "content='" + content + '\'' +
                ", role='" + role + '\'' +
                ", choices=" + choices +
                ", created=" + created +
                ", id='" + id + '\'' +
                ", model='" + model + '\'' +
                ", request_id='" + request_id + '\'' +
                ", usage=" + usage.toString();
    }
}