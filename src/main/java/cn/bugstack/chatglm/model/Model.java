package cn.bugstack.chatglm.model;

public enum Model {
    GLM_3_5_TURBO("glm-3-turbo","适用于对知识量、推理能力、创造力要求较高的场景"),

    GLM_4V_PLUS("glm-4v-plus","适用于对知识量、推理能力、创造力要求较高的场景"),

    COGVIEW_3_PLUS("cogview-3-plus","适用于图像生成任务，通过对用户文字描述快速、精准的理解，让AI的图像表达更加精确和个性化"),

    COGVIDEOX("cogvideox","智谱AI开发的视频生成大模型，具备强大的视频生成能力，只需输入文本或图片就可以轻松完成视频制作");
    private String model;
    private String info;

    private Model(String model, String info) {
        this.model = model;
        this.info = info;
    }

    public static Model getModel(String model) {
        for (Model value : Model.values()) {
            if (value.getModel().equals(model)) {
                return value;
            }
        }
        return null;
    }

    public String getModel() {
        return model;
    }

    public String getInfo() {
        return info;
    }
}
