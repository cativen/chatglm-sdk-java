package cn.bugstack.chatglm.model;

import java.util.Arrays;
import java.util.List;

public class ChatCompletionRequest {
    private String model;
    private List<Message> messages;
    private Boolean stream = true;
    private Model aiModel;
    // 构造函数
    public ChatCompletionRequest(String model, List<Message> messages) {
        this.model = model;
        this.messages = messages;
    }

    public ChatCompletionRequest(Model aiModel, List<Message> messages) {
        this.model = aiModel.getModel();;
        this.messages = messages;
        this.aiModel = aiModel;
    }

    public ChatCompletionRequest(){}

    public void setAiModel(Model aiModel) {
        this.model = aiModel.getModel();
        this.aiModel = aiModel;
    }

    public Model getAiModel() {
        return aiModel;
    }
    // Getter和Setter方法
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public Boolean getStream() {
        return stream;
    }

    public void setStream(Boolean stream) {
        this.stream = stream;
    }

    // 内部类Message
    public static class Message {
        private String role;
        private String content;

        // 构造函数
        public Message(String role, String content) {
            this.role = role;
            this.content = content;
        }

        // Getter和Setter方法
        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        // toString方法
        @Override
        public String toString() {
            return "Message{" +
                    "role='" + role + '\'' +
                    ", content='" + content + '\'' +
                    '}';
        }
    }

    // toString方法
    @Override
    public String toString() {
        return "ChatCompletionRequest{" +
                "model='" + model + '\'' +
                ", messages=" + messages +
                '}';
    }

    // 主方法，用于测试
    public static void main(String[] args) {
        Message message = new Message("user", "什么是社会主义");
        ChatCompletionRequest query = new ChatCompletionRequest("glm-4-plus", Arrays.asList(message));

        System.out.println(query);
    }
}
