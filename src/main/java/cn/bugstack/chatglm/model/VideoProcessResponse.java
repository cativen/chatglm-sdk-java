package cn.bugstack.chatglm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class VideoProcessResponse {

    @Getter
    @Setter
    private String id;

    @Getter
    @Setter
    private String model;

    @Getter
    @Setter
    private String task_status;

    @Getter
    @Setter
    private String request_id;

}
