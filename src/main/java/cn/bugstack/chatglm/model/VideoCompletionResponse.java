package cn.bugstack.chatglm.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;

@NoArgsConstructor
public class VideoCompletionResponse {
    private String model;
    private String request_id;
    private String task_status;
    private List<VideoResult> video_result;

    // 构造函数
    public VideoCompletionResponse(String model, String request_id, String task_status, List<VideoResult> video_result) {
        this.model = model;
        this.request_id = request_id;
        this.task_status = task_status;
        this.video_result = video_result;
    }

    // Getter和Setter方法
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public String getTask_status() {
        return task_status;
    }

    public void setTask_status(String task_status) {
        this.task_status = task_status;
    }

    public List<VideoResult> getVideo_result() {
        return video_result;
    }

    public void setVideo_result(List<VideoResult> video_result) {
        this.video_result = video_result;
    }

    // 内部类VideoResult
    public static class VideoResult {
        private String cover_image_url;
        private String url;

        // 构造函数
        public VideoResult(String cover_image_url, String url) {
            this.cover_image_url = cover_image_url;
            this.url = url;
        }

        // Getter和Setter方法
        public String getCover_image_url() {
            return cover_image_url;
        }

        public void setCover_image_url(String cover_image_url) {
            this.cover_image_url = cover_image_url;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        // toString方法
        @Override
        public String toString() {
            return "VideoResult{" +
                    "cover_image_url='" + cover_image_url + '\'' +
                    ", url='" + url + '\'' +
                    '}';
        }
    }

    // toString方法
    @Override
    public String toString() {
        return "VideoApiResponse{" +
                "model='" + model + '\'' +
                ", request_id='" + request_id + '\'' +
                ", task_status='" + task_status + '\'' +
                ", video_result=" + video_result +
                '}';
    }

    // 主方法，用于测试
    public static void main(String[] args) {
        VideoResult videoResult = new VideoResult(
                "https://aigc-files.bigmodel.cn/api/cogvideo/11165458-d871-11ef-9d2e-6a6b9fbd524e_cover_0.jpeg",
                "https://aigc-files.bigmodel.cn/api/cogvideo/11165458-d871-11ef-9d2e-6a6b9fbd524e_0.mp4"
        );
        VideoCompletionResponse response = new VideoCompletionResponse(
                "cogvideox",
                "-9039024068128152717",
                "SUCCESS",
                Arrays.asList(videoResult)
        );

        System.out.println(response);
    }
}

