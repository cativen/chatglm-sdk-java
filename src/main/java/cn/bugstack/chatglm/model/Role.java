package cn.bugstack.chatglm.model;

public enum Role {

    USER("user"),
    ASSISTANT("assistant"),
    SYSTEM("system");

    private String role;

    Role(String role) {
        this.role = role;
    }

    public String getRole() {
        return role;
    }
}
