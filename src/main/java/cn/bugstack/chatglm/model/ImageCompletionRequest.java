package cn.bugstack.chatglm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class ImageCompletionRequest {
    @Getter
    @Setter
    private String model;
    @Getter
    @Setter
    private String prompt;
    @Getter
    private Model aiModel;

    public ImageCompletionRequest(Model aiModel, String prompt) {
        this.model = aiModel.getModel();
        this.aiModel = aiModel;
        this.prompt = prompt;
    }
}
