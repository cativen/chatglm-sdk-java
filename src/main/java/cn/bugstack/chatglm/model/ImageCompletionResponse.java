package cn.bugstack.chatglm.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
public class ImageCompletionResponse {
    private long created;

    private List<Url> data;

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public List<Url> getData() {
        return data;
    }

    public void setData(List<Url> data) {
        this.data = data;
    }

    public List<String> getUrls() {
        return data.stream().map(Url::getUrl).collect(Collectors.toList());
    }
    class Url {
        private String url;

        public String getUrl() {
            return url;
        }
        public void setUrl(String url) {
            this.url = url;
        }
    }
}
