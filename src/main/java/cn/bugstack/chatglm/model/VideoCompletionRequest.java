package cn.bugstack.chatglm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class VideoCompletionRequest {

    @Getter
    @Setter
    private String model;
    @Getter
    @Setter
    private String prompt;
    @Getter
    private Model aiModel;

    public VideoCompletionRequest(Model aiModel, String prompt) {
        this.model = aiModel.getModel();
        this.aiModel = aiModel;
        this.prompt = prompt;
    }

}
