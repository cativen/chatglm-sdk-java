import cn.bugstack.chatglm.model.ChatCompletionRequest;
import cn.bugstack.chatglm.model.ChatCompletionResponse;
import cn.bugstack.chatglm.model.Model;
import cn.bugstack.chatglm.model.Role;
import cn.bugstack.chatglm.session.Configuration;
import cn.bugstack.chatglm.session.OpenAiSessionFactory;
import cn.bugstack.chatglm.session.OpenSession;
import cn.bugstack.chatglm.session.defaults.DefaultOpenAiSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ChatFlowTest {

    private static Logger logger = LoggerFactory.getLogger(ChatFlowTest.class);

    public static void main(String[] args) throws IOException {
        // 创建一个 Configuration 对象
        Configuration configuration = new Configuration();
        // 设置你的 API Secret Key
        configuration.setApiSecretKey("e64bbd89200540b08873d5be17582312.48J1rFfpnm1JIFGc");
        // 设置你的 API Base URL
        configuration.setApiHost("https://open.bigmodel.cn/");
        OpenAiSessionFactory factory = new DefaultOpenAiSessionFactory(configuration);
        OpenSession openSession = factory.openSession();
        System.out.println("我是 智谱清言AI大模型，请输入你的问题：");

        ChatCompletionRequest chatCompletionRequest = new ChatCompletionRequest();
        chatCompletionRequest.setAiModel(Model.GLM_4V_PLUS);
        List<ChatCompletionRequest.Message> messageList = new ArrayList<>();

        // 3. 等待输入
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String text = scanner.nextLine();
            logger.info("用户输入:{}", text);
            ChatCompletionRequest.Message message = new ChatCompletionRequest.Message(Role.USER.getRole(), text);
            messageList.add(message);
            chatCompletionRequest.setMessages(messageList);
            ChatCompletionResponse chatCompletionResponse = openSession.chatCompletion(chatCompletionRequest);
            logger.info("对话得到的结果:{}", chatCompletionResponse.getChoices().get(0).getMessage().getContent());
        }
    }
}